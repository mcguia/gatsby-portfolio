const theme = {
  colors: {
    lightGrey: "#9e9e9e",
    darkGrey: "#212121",
    yellow: "#ffe810",
  },

  fonts: {
    HKGrotesk:
      "HK Grotesk, San Francisco, SF Pro Text, -apple-system, system-ui, BlinkMacSystemFont, Roboto, Helvetica Neue, Segoe UI, Arial, sans-serif",
    Lato: "Lato, Lucida Grande, Tahoma, sans-serif",
    ApercuRegular: "Apercu Regular, Lucida Grande, Tahoma, sans-serif",
    ApercuMono: "Apercu Mono, Monaco, monospace",
  },

  fontSizes: {
    xxxl: "4rem",
    xxl: "3rem",
    xl: "2.5rem",
    lg: "2rem",
    md: "1.5rem",
    sm: "1.25rem",
    xs: "1rem",
    xxs: "0.75rem",
  },

  easing: "cubic-bezier(0.645, 0.045, 0.355, 1)",
  transition: "all 0.25s cubic-bezier(0.645, 0.045, 0.355, 1)",
}

export default theme
